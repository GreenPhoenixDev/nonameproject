﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponparticleEffectsPools : MonoBehaviour
{
	[Header("Assault Rifle")]
	[SerializeField] private GameObject arHit;
	[SerializeField] private GameObject arTrail;
	[SerializeField] private int arPoolSize;
	[Space]
	[Header("Shotgun")]
	[SerializeField] private GameObject shotgunHit;
	[SerializeField] private GameObject shotguntrail;
	[SerializeField] private int shotgunPoolSize;
	[Space]
	[Header("Long Shot")]
	[SerializeField] private GameObject longShotHit;
	[SerializeField] private GameObject longShotTrail;
	[SerializeField] private int longshotPoolSize;

	[HideInInspector] public Stack<GameObject> AssaultRifleHitEffects;
	[HideInInspector] public Stack<GameObject> AssaultRifleTrails;

	[HideInInspector] public Stack<GameObject> ShotgunHitEffects;
	[HideInInspector] public Stack<GameObject> ShotgunTrails;

	[HideInInspector] public Stack<GameObject> LongshotHitEffects;
	[HideInInspector] public Stack<GameObject> LongshotTrails;

	// fill all stacks with objects
	void Awake()
	{
		// ASSAULT RIFLE
		AssaultRifleHitEffects = new Stack<GameObject>(arPoolSize);
		AssaultRifleTrails = new Stack<GameObject>(arPoolSize);
		for (int i = 0; i < arPoolSize; i++)
		{
			var arHitGO = Instantiate(arHit, transform);
			arHitGO.SetActive(false);
			AssaultRifleHitEffects.Push(arHitGO);

			var arTrailGO = Instantiate(arTrail, transform);
			arTrailGO.SetActive(false);
			AssaultRifleTrails.Push(arTrailGO);
		}

		// SHOTGUN
		ShotgunHitEffects = new Stack<GameObject>(shotgunPoolSize);
		ShotgunTrails = new Stack<GameObject>(shotgunPoolSize);
		for (int i = 0; i < shotgunPoolSize; i++)
		{
			var shotgunHitEffect = Instantiate(shotgunHit, transform);
			shotgunHitEffect.SetActive(false);
			ShotgunHitEffects.Push(shotgunHitEffect);

			var shotgunTrailGO = Instantiate(shotguntrail, transform);
			shotgunTrailGO.SetActive(false);
			ShotgunTrails.Push(shotgunTrailGO);
		}

		// LONGSHOT
		LongshotHitEffects = new Stack<GameObject>(longshotPoolSize);
		LongshotTrails = new Stack<GameObject>(longshotPoolSize);
		for (int i = 0; i < longshotPoolSize; i++)
		{
			var longShotHitEffect = Instantiate(longShotHit, transform);
			longShotHitEffect.SetActive(false);
			LongshotHitEffects.Push(longShotHitEffect);

			var longShotTrailGO = Instantiate(longShotTrail, transform);
			longShotTrailGO.SetActive(false);
			LongshotTrails.Push(longShotTrailGO);
		}
	}
}
