﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Weapon : MonoBehaviour
{
	public FireModes CurrentFireMode = FireModes.SingleFire;

	[Header("Hit Settings")]
	[SerializeField] protected LayerMask hitLayer;
	[SerializeField] protected GameObject hitEffect;
	[Space][Header("Shooting Settings")]
	[SerializeField] protected GameObject shootingEffect;
	[SerializeField] protected GameObject reloadEffect;
	[SerializeField] protected Transform firePoint;
	[SerializeField] protected float spray;
	[SerializeField] protected float damage;
	[SerializeField] protected float headshotMultiplier;
	[SerializeField] protected float reloadTime; // maybe not needed if you play the animation and disable shooting while it is running
	[SerializeField] protected float bulletRange;
	[Space][Header("Sounds")]
	[SerializeField] protected AudioClip shootingSound;
	[SerializeField] protected AudioClip reloadSound;
	[SerializeField] protected AudioClip defaultHitSound;
	[SerializeField] protected AudioClip humanHitSound;
	[Space][Header("Third Person Settings")]
	[SerializeField] protected GameObject thirdPersonWeapon;
	[SerializeField] protected GameObject secondShootingEffect;
	[SerializeField] protected GameObject secondReloadEffect;
	[SerializeField] protected Transform secondFirePoint;

	protected PlayerManager playerManager;
	protected PlayerWeaponControls playerWeaponControls;
	protected WeaponparticleEffectsPools weaponparticleEffectsPools;
	protected PlayerUI playerUI;
	protected Transform rayCastTransformPoint;
	protected AudioSource source;
	protected WaitForSeconds wfs = new WaitForSeconds(1f);

	[Space][Header("Properties")]
	[SerializeField] protected Sprite crossHair;
	public Sprite CrossHair { get => crossHair; }
	[SerializeField] protected Image weaponsUIImage;
	public Image WeaponsUIImage { get => weaponsUIImage; }
	[SerializeField] protected float fireRate;
	public float FireRate { get => fireRate; }
	[SerializeField] protected int maxAmmo;
	public int MaxAmmo { get => maxAmmo; }
	protected int currentMaxAmmo;
	public int CurrentMaxAmmo { get => currentMaxAmmo; }
	[SerializeField] protected int magazineSize;
	public int MagazineSize { get => magazineSize; }
	protected int currentAmmo;
	public int CurrentAmmo { get => currentAmmo; }
	[SerializeField] protected float changeFOVTime = 1f;
	public float ChangeFOVTime { get => changeFOVTime; }
	protected bool isReloading;
	public bool IsReloading { get => isReloading; }
	protected int weaponIdx;

	public abstract void ShootOneTime();

	#region Public Methods
	public void InitWeapon(PlayerManager _playerManager, PlayerWeaponControls _playerWeaponControls, WeaponparticleEffectsPools _weaponparticleEffectsPools, PlayerUI _playerUI, int idx)
	{
		// SET SOME WEAPON DEFAULTS
		playerManager = _playerManager;
		playerWeaponControls = _playerWeaponControls;
		weaponparticleEffectsPools = _weaponparticleEffectsPools;
		playerUI = _playerUI;
		rayCastTransformPoint = playerManager.PlayerCam.transform;
		currentAmmo = magazineSize;
		weaponIdx = idx;
		source = GetComponent<AudioSource>();
	}
	public void Reload()
	{
		if (currentMaxAmmo == 0 || isReloading || currentAmmo == magazineSize)
		{
			// PLAY A CLICKING SOUND AND DISPLAY A MESSAGE
			Debug.Log("Can't reload, no ammo left / or magazine is full");
			return;
		}
		StartCoroutine(Reloading());
	}
	public void ReloadOnDeath()
	{
		currentMaxAmmo -= magazineSize - currentAmmo;
		currentAmmo = magazineSize;
	}
	public bool RefillMaxAmmo()
	{
		if (currentMaxAmmo == maxAmmo)
		{
			return false;
		}
		else
		{
			currentMaxAmmo += magazineSize;
			Mathf.Clamp(currentMaxAmmo, 0, maxAmmo);
			return true;
		}
	}
	public void ResetCurMaxAmmo()
	{
		currentMaxAmmo = 0;
		currentAmmo = magazineSize;
	}
	public void DeactivateThirdPersonWeaponInstance()
	{
		thirdPersonWeapon.SetActive(false);
	}
	public void ActivateThirdPersonWeaponInstance()
	{
		thirdPersonWeapon.SetActive(true);
	}
	protected Vector3 GetSpray()
	{
		float randomXSpray = Random.Range(-spray, spray);
		float randomYSpray = Random.Range(-spray, spray);
		float randomZSpray = Random.Range(-spray, spray);
		Vector3 direction = rayCastTransformPoint.forward;
		direction.x += randomXSpray;
		direction.y += randomYSpray;
		direction.z += randomZSpray;

		return direction;
	}
	protected void UseHitEffect(Stack<GameObject> pool, Vector3 hitNormal, Vector3 pos)
	{
		var hitVFX = pool.Pop();
		hitVFX.SetActive(true);
		hitVFX.transform.parent = null;
		Quaternion effectRotation = Quaternion.LookRotation(hitNormal);
		hitVFX.transform.rotation = effectRotation;
		hitVFX.transform.position = pos + hitVFX.transform.forward * 0.1f;
		playerManager.WC.ResetParticleItem(hitVFX, pool, wfs, weaponparticleEffectsPools);
	}
	protected void UseLightTrail(Stack<GameObject> pool, Transform firePoint, Vector3 hitPoint, string layer)
	{
		var lightTrail = pool.Pop();
		lightTrail.layer = LayerMask.NameToLayer(layer);
		lightTrail.SetActive(true);
		lightTrail.transform.parent = null;
		lightTrail.transform.position = firePoint.position;
		lightTrail.transform.LookAt(hitPoint);
		playerManager.WC.ResetParticleItem(lightTrail, pool, wfs, weaponparticleEffectsPools);
	}
	#endregion

	#region Coroutines
	protected IEnumerator Reloading()
	{
		isReloading = true;

		source.PlayOneShot(reloadSound);

		reloadEffect.SetActive(true);
		secondReloadEffect.SetActive(true);

		yield return new WaitForSeconds(reloadTime);

		if (currentMaxAmmo < magazineSize)
		{
			currentAmmo = currentMaxAmmo;
			currentMaxAmmo = 0;
		}
		else
		{
			currentMaxAmmo -= magazineSize - currentAmmo;
			currentAmmo = magazineSize;
		}

		reloadEffect.SetActive(false);
		secondReloadEffect.SetActive(false);
		isReloading = false;
		playerUI.ReloadWeapon(weaponIdx);
	}
	#endregion
}

public enum FireModes { SingleFire, FullAuto }