﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCoroutines : MonoBehaviour
{
	public void ShootEffect(GameObject shootingEffect, GameObject secondShootingEffect, float fireRate)
	{
		StartCoroutine(ShootEffectRoutine(shootingEffect, secondShootingEffect, fireRate));
	}
	public void ResetParticleItem(GameObject effect, Stack<GameObject> pool, WaitForSeconds wfs, WeaponparticleEffectsPools weaponparticleEffectsPools)
	{
		StartCoroutine(ResetParticleItemRoutine(effect, pool, wfs, weaponparticleEffectsPools));
	}

	IEnumerator ShootEffectRoutine(GameObject shootingEffect, GameObject secondShootingEffect, float fireRate)
	{
		Debug.Log("started ShootEffect");
		shootingEffect.SetActive(true);
		secondShootingEffect.SetActive(true);

		yield return new WaitForSeconds((1 / fireRate) * 0.8f);

		Debug.Log("stopped ShootEffect");
		shootingEffect.SetActive(false);
		secondShootingEffect.SetActive(false);
	}
	IEnumerator ResetParticleItemRoutine(GameObject effect, Stack<GameObject> pool, WaitForSeconds wfs, WeaponparticleEffectsPools weaponparticleEffectsPools)
	{
		//Debug.Log("started ResetParticleItem");
		yield return wfs;

		effect.SetActive(false);
		effect.transform.parent = weaponparticleEffectsPools.transform;
		pool.Push(effect);

	}
}