﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour // maybe inherit from sth like InputManager (controls which input state is active right now)
{
	#region Script References
	private PlayerManager playerManager;
	private PlayerMovement playerMovement;
	private PlayerWeaponControls playerWeaponControl;
	private PlayerHealthControl playerHealthControl;
	#endregion
	#region Input Strings
	[SerializeField] private string moveString = "VerticalP";
	[SerializeField] private string strafeString = "HorizontalP";
	[SerializeField] private string lookUpString = "LookUpP";
	[SerializeField] private string turnString = "TurnAroundP";
	[SerializeField] private string jumpString = "JumpP";
	[SerializeField] private string shootString = "ShootP";
	[SerializeField] private string reloadString = "ReloadP";
	[SerializeField] private string switchWeaponString = "SwitchWeaponP";
	[SerializeField] private string changeFireModeString = "ChangeWeaponModeP";
	[SerializeField] private string optionsMenuString = "OptionsMenuP";
	#endregion
	#region Variables
	[Header("Input Options")]
	[SerializeField] private float shootDeadZone = 0.9f;
	[SerializeField] private bool isGrounded;
	[Header("Animator")]
	[SerializeField] private Animator anim;
	private WaitForSeconds wfs = new WaitForSeconds(0.2f);
	private Vector3[] rayOffsets;
	private bool isFalling;
	private bool startedShooting;
	private bool canShootAgain;
	private bool waitABitPlease;
	private bool oldIsGrounded;
	private bool hitGround;
	private bool leftGround;
	#endregion

	#region Player Standard Methods
	public void Init(PlayerManager _playerManager, PlayerMovement _playerMovement, PlayerWeaponControls _playerWeaponControls, PlayerHealthControl _playerHealthControl)
	{
		// set the PlayerManager instance
		playerManager = _playerManager;
		playerMovement = _playerMovement;
		playerWeaponControl = _playerWeaponControls;
		playerHealthControl = _playerHealthControl;

		Debug.Log("PlayerInput has succesfully been initialised");
	}
	public void OnUpdate()
	{
		if (!playerHealthControl.IsDead)
		{
			CalculateMovementInput();
			CalculateRotationInput();
			oldIsGrounded = isGrounded;
			isGrounded = GroundCheck();
			CheckBackToGround();
			anim.SetBool("IsGrounded", isGrounded);
			playerMovement.SetGrounded(isGrounded);
			Jump();
			Shoot();
			Reload();
			SwitchWeapon();
			ChangeFireMode();
		}

		OpenOptionsMenu();
	}
	#endregion

	#region OnUpdate
	void CalculateMovementInput()
	{
		float yMovement = Input.GetAxis(moveString);
		float xMovement = Input.GetAxis(strafeString);
		anim.SetFloat("VerticalSpeed", yMovement);
		anim.SetFloat("HorizontalSpeed", xMovement);

		float moveFactor = Mathf.Abs(yMovement);
		if(Mathf.Abs(xMovement) > moveFactor) { moveFactor = Mathf.Abs(xMovement); }
		moveFactor = Mathf.Clamp(moveFactor, playerManager.MinMoveFactor, 1f);

		// deadzone is set in the input settings! no check needed
		Vector3 moveVertical = transform.forward * yMovement * playerManager.VerticalSpeed;
		Vector3 moveHorizontal = transform.right * xMovement * playerManager.HorizontalSpeed;

		Vector3 moveDirection = (moveVertical + moveHorizontal).normalized;

		playerMovement.GetMovement(moveDirection, moveFactor);
	}
	void CalculateRotationInput()
	{
		// player y rotation
		float yRot = Input.GetAxisRaw(turnString);

		Vector3 rotation = new Vector3(0f, yRot, 0f) * playerManager.YJoystickSensitivity;

		playerMovement.GetPlayerRotation(rotation);

		// camera x rotation
		float xRot = Input.GetAxisRaw(lookUpString);

		float cameraXRotation = xRot * playerManager.XJoystickSensitivity;

		playerMovement.GetCameraRotation(cameraXRotation);
	}
	bool GroundCheck()
	{


		// RayCast in the center
		Debug.DrawRay(playerManager.RayCastEmpty.transform.position + Vector3.up * playerManager.RayCastYOffset, Vector3.down * playerManager.RayCastLength, Color.red, 1f);
		if (Physics.Raycast(
			playerManager.RayCastEmpty.transform.position + Vector3.up * playerManager.RayCastYOffset,
			Vector3.down,
			playerManager.RayCastLength,
			playerManager.RayCastLayer))
		{
			return true;
		}

		for (int i = 0; i < playerManager.NumOfRays; i++)
		{
			Vector3 rayPosition = playerManager.RayCastEmpty.transform.position + playerManager.RayCastEmpty.transform.forward * playerManager.RayCastRadius;
			Debug.DrawRay(rayPosition + Vector3.up * playerManager.RayCastYOffset, Vector3.down, Color.red, 1f);
			if (Physics.Raycast(
				rayPosition + Vector3.up * playerManager.RayCastYOffset,
				Vector3.down,
				playerManager.RayCastLength,
				playerManager.RayCastLayer))
			{
				return true;
			}
		}

		// return false if none of the rays hit the ground
		return false;
	}
	void CheckBackToGround()
	{
		// needed for checking first hit after in air
		if(!oldIsGrounded && isGrounded)
		{
			// ouch....had to hurt
			hitGround = true;
		}
		else if(oldIsGrounded && !isGrounded)
		{
			// ready.....LIFTOFF
			leftGround = true;
		}
		else
		{
			hitGround = false;
			leftGround = false;
		}
		anim.SetBool("HitGround", hitGround);
	}
	void Jump()
	{
		if (isGrounded && Input.GetButtonDown(jumpString))
		{
			StartCoroutine("StopGroundcheck");
			isGrounded = false;
			anim.SetTrigger("Jump");
			playerManager.PM.Jump();
		}
	}
	void Shoot()
	{
		// return if ammo is empty
		if (playerWeaponControl.CurrentWeapon.CurrentAmmo <= 0)
		{
			// DISPLAY "NEED TO RELOAD" OR MAKE A CLICKING SOUND, EVTL. RELOAD AUTOMATICALLY
			if (startedShooting)
			{
				CancelInvoke("FullAutoShoot");
				startedShooting = false;
			}
			return;
		}

		float shootInput = Input.GetAxisRaw(shootString);

		if (playerWeaponControl.CurrentWeapon.CurrentFireMode == FireModes.FullAuto)
		{
			if (Input.GetAxisRaw(shootString) > shootDeadZone && !startedShooting && !waitABitPlease)
			{
				InvokeRepeating("FullAutoShoot", 0f, 1f / playerWeaponControl.CurrentWeapon.FireRate);
				startedShooting = true;
			}
			else if (Input.GetAxisRaw(shootString) < shootDeadZone && startedShooting)
			{
				CancelInvoke("FullAutoShoot");
				StartCoroutine("FixedTimer");
				startedShooting = false;
			}
		}
		else if (playerWeaponControl.CurrentWeapon.CurrentFireMode == FireModes.SingleFire)
		{
			if (Input.GetAxisRaw(shootString) > shootDeadZone && canShootAgain)
			{
				playerWeaponControl.SingleShoot();
				canShootAgain = false;
			}
			else if (Input.GetAxisRaw(shootString) < shootDeadZone && !canShootAgain)
			{
				canShootAgain = true;
			}
		}
	}
	void FullAutoShoot()
	{
		playerWeaponControl.Shoot();
	}
	void Reload()
	{
		if (Input.GetButtonDown(reloadString))
		{
			playerWeaponControl.Reload();
		}
	}
	void SwitchWeapon()
	{
		if (Input.GetButtonDown(switchWeaponString))
		{
			playerWeaponControl.SwitchWeapon();
		}
	}
	void ChangeFireMode()
	{
		if (Input.GetButtonDown(changeFireModeString))
		{
			playerWeaponControl.ChangeFireMode();
		}
	}
	void OpenOptionsMenu()
	{
		if (Input.GetButtonDown(optionsMenuString))
		{
			playerManager.PUI.SensitivityMenu();
		}
	}
	#endregion

	#region Coroutines
	IEnumerator FixedTimer()
	{
		waitABitPlease = true;

		yield return new WaitForSeconds(1f / playerWeaponControl.CurrentWeapon.FireRate);

		waitABitPlease = false;
	}
	IEnumerator StopGroundcheck()
	{
		float oldLength = playerManager.RayCastLength;
		playerManager.RayCastLength -= 1f;

		yield return wfs;

		playerManager.RayCastLength = oldLength;
	}
	#endregion
}