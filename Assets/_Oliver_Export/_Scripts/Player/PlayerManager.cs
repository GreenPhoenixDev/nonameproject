﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerWeaponControls), typeof(PlayerReset), typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerEnvironmentInteraction), typeof(PlayerInput), typeof(Rigidbody))]
[RequireComponent(typeof(PlayerUI), typeof(PlayerHealthControl))]
public class PlayerManager : MonoBehaviour
{
	#region Script References / Auto Properties
	[Header("Settings for GAME")]
	[SerializeField] private int playerIdx;
	public int PlayerIdx { get => playerIdx; }
	[SerializeField] private GAME game;
	public GAME Game { get => game; }
	private PlayerMovement pM;
	public PlayerMovement PM { get => pM; }
	private PlayerWeaponControls pWC;
	public PlayerWeaponControls PWC1 { get => pWC; }
	private PlayerReset pR;
	public PlayerReset PR { get => pR; }
	private PlayerEnvironmentInteraction pEI;
	public PlayerEnvironmentInteraction PEI { get => pEI; }
	private PlayerInput pI;
	public PlayerInput PI { get => pI; }
	private PlayerUI pUI;
	public PlayerUI PUI { get => pUI; }
	private PlayerHealthControl pHC;
	public PlayerHealthControl PHC { get => pHC; }
	[SerializeField] private WeaponCoroutines wC;
	public WeaponCoroutines WC { get => wC; }
	[Space][Header("Spawner")]
	[SerializeField] private PlayerSpawner spawner;
	public PlayerSpawner Spawner { get => spawner; }
    #endregion
    #region Public Player Variables
    [Space][Header("Player Menu")]
    [SerializeField] private Slider sliderX;
    [SerializeField] private Slider sliderY;
	[Space][Header("Joystick Options")]
	[SerializeField] private float horizontalSpeed = 15f;
	public float HorizontalSpeed { get => horizontalSpeed; }
	[SerializeField] private float verticalSpeed = 10f;
	public float VerticalSpeed { get => verticalSpeed; }
	[SerializeField] private float cameraRotationLimit = 75f;
	public float CameraRotationLimit { get => cameraRotationLimit; }
	[SerializeField] private float yJoystickSensitivity = 3f;
	public float YJoystickSensitivity { get => yJoystickSensitivity; set => yJoystickSensitivity = value; }
	[SerializeField] private float xJoystickSensitivity = 1.5f;
	public float XJoystickSensitivity { get => xJoystickSensitivity; set => xJoystickSensitivity = value; }

	[Space][Header("Movement Options")]
	[SerializeField] private float maxSpeed = 40f;
	public float MaxSpeed { get => maxSpeed; }
	[SerializeField] private float acceleration = 10f;
	public float Acceleration { get => acceleration; }
	[SerializeField] private float friction = 0.2f;
	public float Friction { get => friction; }
	[SerializeField] private float minMoveFactor = 0.2f;
	public float MinMoveFactor { get => minMoveFactor; }
	[SerializeField] private float jumpForce;
	public float JumpForce { get => jumpForce; }
	[SerializeField] private float inAirFactor = 0.1f;
	public float InAirFactor { get => inAirFactor; }

	[Space][Header("Camera Options")]
	[SerializeField] private Camera playerCam;
	public Camera PlayerCam { get => playerCam; }
	[SerializeField] private float defaultFOV = 80f;
	public float DefaultFOV { get => defaultFOV; }
	[SerializeField] private float maxFOV = 85f;
	public float MaxFOV { get => maxFOV; }

	[Space][Header("GroundCheck Options")]
	[SerializeField] private GameObject rayCastEmpty;
	public GameObject RayCastEmpty { get => rayCastEmpty; }
	[SerializeField] private int numOfRays;
	public int NumOfRays { get => numOfRays; }
	[SerializeField] private float rayCastRadius;
	public float RayCastRadius { get => rayCastRadius; }
	[SerializeField] private float rayCastYOffset;
	public float RayCastYOffset { get => rayCastYOffset; }
	[SerializeField] private float rayCastLength;
	public float RayCastLength { get => rayCastLength; set => rayCastLength = value; }
	[SerializeField] private LayerMask rayCastLayer;
	public LayerMask RayCastLayer { get => rayCastLayer; }
	[SerializeField] private float switchWeaponTime;
	public float SwitchWeaponTime { get => switchWeaponTime; }

	[Space][Header("LayerStrings")]
	[SerializeField] private string selfLayer;
	public string SelfLayer { get => selfLayer; }
	[SerializeField] private string otherPlayerLayer;
	public string OtherPlayerLayer { get => otherPlayerLayer; }
	#endregion
	#region Private Player Variables
	private Rigidbody rb;
	private float playerCamCurrentRotation;
	#endregion

	#region Monobehaviour
	void Awake()
	{
	}
	void Start()
	{
		InitPlayer();
	}
	void Update()
	{
		// update components
		PM.OnUpdate();
		PR.OnUpdate();
		pWC.OnUpdate();
		PEI.OnUpdate();
		PI.OnUpdate();
		pUI.OnUpdate();
	}
	#endregion

	#region InitPlayer
	void InitPlayer()
	{
		// get all components
		rb = GetComponent<Rigidbody>();
		pM = GetComponent<PlayerMovement>();
		pR = GetComponent<PlayerReset>();
		pWC = GetComponent<PlayerWeaponControls>();
		pEI = GetComponent<PlayerEnvironmentInteraction>();
		pI = GetComponent<PlayerInput>();
		pUI = GetComponent<PlayerUI>();
		pHC = GetComponent<PlayerHealthControl>();

		// initialize all components
		pM.Init(this, rb, playerCam);
		pR.Init(this, pHC, pWC, pUI, game);
		pWC.Init(this, pUI);
		PEI.Init(pWC, pHC, pM, pUI);
		PI.Init(this, PM, pWC, pHC);
		pUI.Init(pHC, pWC, game);
		pHC.Init(pR, pWC, pUI);

        // initialize sliders
        sliderX.value = xJoystickSensitivity;
        sliderY.value = yJoystickSensitivity;
	}
	#endregion
}
