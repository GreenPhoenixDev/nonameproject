﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReset : MonoBehaviour
{
	#region Script References
	private PlayerManager playerManager;
	private PlayerHealthControl playerHealthControl;
	private PlayerWeaponControls playerWeaponControls;
	private PlayerUI playerUI;
	private GAME game;
	#endregion

	#region Player Standard Methods
	public void Init(PlayerManager _playerManager, PlayerHealthControl _playerHealthControl, PlayerWeaponControls _playerWeaponControls, PlayerUI _playerUI, GAME _game)
	{
		// set defaults
		playerManager = _playerManager;
		playerHealthControl = _playerHealthControl;
		playerWeaponControls = _playerWeaponControls;
		playerUI = _playerUI;
		game = _game;
		Debug.Log("PlayerReset has succesfully been initialised");
	}
	public void OnUpdate()
	{

	}
	#endregion

	#region Player Died
	public void ResetOnStart()
	{
		playerWeaponControls.ResetOnStart();
		OnPlayerDeath();
        playerUI.ResetWeaponAmmo();
	}
	public void OnPlayerDeath()
	{
		game.KillCounter(playerManager.PlayerIdx);
		playerHealthControl.RestoreHealth();
		RespawnPlayer();
		playerUI.PlayerHealthUI();
		playerUI.PlayerWeaponUI();
	}
	void RespawnPlayer()
	{
		Transform spawnPos = playerManager.Spawner.SpawnPlayer();
		transform.position = spawnPos.position;
	}
	#endregion
}
